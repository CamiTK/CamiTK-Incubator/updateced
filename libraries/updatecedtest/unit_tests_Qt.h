#include <QtTest/QtTest>

#include "CatalogueEdition_unit_tests_API.h"

class CEPInspection;

class CATALOGUEEDITION_UNIT_TESTS_API TestCepInspection : public QObject
{
	Q_OBJECT
public:
	TestCepInspection();
	virtual ~TestCepInspection();

	private slots:
	//void findFilesNotFind();

	//void parseCMakeFile();

	//void loadXMLFile();

	//void createActionExtension();

	//void createComponentExtension();

	//void createMap();

	//void loadDll();

	void testAll();

private:
	CEPInspection* mCepInspection;
	
};

