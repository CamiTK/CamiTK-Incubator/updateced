#include "unit_tests_Qt.h"

#include <CEPInspection.h>
#include <CMakeListsFilesParser.h>
#include <ActionExtC2ActionExtS.h>
#include <ComponentExtC2ComponentExtS.h>

#include <ExtensionManager.h>

#include <QDebug>


TestCepInspection::TestCepInspection()
{
	mCepInspection = new CEPInspection();

	mCepInspection->setPathSrc("C:\\dev\\camitk\\src\\modeling");
	//mCepInspection->setPathBuild("");
	mCepInspection->setPathBuild("C:\\dev\\camitk\\build");
}

TestCepInspection::~TestCepInspection()
{
	delete mCepInspection;
	mCepInspection = NULL;
}


//void TestCepInspection::findFilesNotFind()
//{
//	QStringList list;
//	mCepInspection->setPathSrc("");
//	mCepInspection->findFileInDirectory("CMakeLists.txt", mCepInspection->getPathSrc(), list);
//
//	QVERIFY(0 == list.size());
//}
//
//void TestCepInspection::parseCMakeFile()
//{
//	mCepInspection->parseCmakeListFile("C:\\Users\\seryc\\Documents\\Travail\\Conversion\\src\\libraries\\camitk2cepcoreschema_unit_tests\\CMakeLists.txt");
//}
//
//void TestCepInspection::loadXMLFile()
//{
//	mCepInspection->setPathSrc("C:\\Users\\seryc\\Desktop\\Wizard camitk 4\\FirstCep\\afirstcep");
//	mCepInspection->findFileExistingCEPContent();
//	//QVERIFY(true == mCepInspection->loadExistingCepContentFile());
//}

//void TestCepInspection::createActionExtension()
//{
//	CMakeListsFilesParser* parser = mCepInspection->parseCmakeListFile("C:\\Users\\seryc\\Desktop\\Wizard camitk 4\\FirstCep\\withCEPGenerator\\afirstcep\\actions\\firstactionextension\\CMakeLists.txt");
//
//	camitk::ExtensionManager::loadExtension(camitk::ExtensionManager::ACTION, "C:\\Users\\seryc\\Desktop\\Wizard camitk 4\\FirstCep\\withCEPGenerator\\build\\lib\\camitk-4.0\\actions\\firstactionextension-debug.dll");
//
//	QList<camitk::ActionExtension*> listAction = camitk::ExtensionManager::getActionExtensionsList();
//
//	qDebug() << "TestCepInspection::createActionExtension()" << "size of listActionExtension: " << listAction.size();
//
//	ActionExtC2ActionExtS* actExt = new ActionExtC2ActionExtS();
//	mCepInspection->updateActionExtension(actExt, parser, listAction.at(0));
//
//	camitk::ExtensionManager::unloadAllActionExtensions();
//
//	QVERIFY(true == true);
//}

//void TestCepInspection::createComponentExtension()
//{
//	CMakeListsFilesParser* parser = mCepInspection->parseCmakeListFile("C:\\Users\\seryc\\Desktop\\Wizard camitk 4\\FirstCep\\withCEPGenerator\\afirstcep\\components\\firstcomponentextension\\CMakeLists.txt");
//
//	camitk::ExtensionManager::loadExtension(camitk::ExtensionManager::COMPONENT, "C:\\Users\\seryc\\Desktop\\Wizard camitk 4\\FirstCep\\withCEPGenerator\\build\\lib\\camitk-4.0\\components\\firstcomponentextension-debug.dll");
//
//	QList<camitk::ComponentExtension*> listComponent = camitk::ExtensionManager::getComponentExtensionsList();
//
//	qDebug() << "TestCepInspection::createComponentExtension()" << "size of listComponentExtension: " << listComponent.size();
//
//	ComponentExtC2ComponentExtS* compExt = new ComponentExtC2ComponentExtS();
//	mCepInspection->updateComponentExtension(compExt, parser, listComponent.at(0));
//
//	QVERIFY(true == true);
//}

//void TestCepInspection::createMap()
//{
//	qDebug() << "Test Create Map: getPathSrc() : " << mCepInspection->getPathSrc() << " getPathBuild: " << mCepInspection->getPathBuild();
//	qDebug() << "Test Create Map: mCepInspection->findFileCMakeLists() : " << mCepInspection->findFileCMakeLists();
//	qDebug() << "Test Create Map: mCepInspection->findFileDynamicLibraries() : " << mCepInspection->findFileDynamicLibraries();
//	mCepInspection->buildCorrespondanciesMap();
//	qDebug() << "Test Create Map: mCepInspection->getMap().count(): " << mCepInspection->getMap().count(); 
//	QVERIFY(mCepInspection->getMap().isEmpty() != true);
//}
//
//void TestCepInspection::loadDll()
//{
//	// Suppose that the map is created.
//	int res = 0;
//	if (mCepInspection->getMap().isEmpty() != true)
//	{
//		for each (QFileInfo var in mCepInspection->getMap().values())
//		{
//			if (mCepInspection->loadExistingDynamicLibraries(var))
//			{
//				res = res + 1;
//			}
//		}
//		
//	}
//	QVERIFY(res>0);
//}
//
void TestCepInspection::testAll()
{
	bool res = mCepInspection->process();
	QVERIFY(res == true);
}