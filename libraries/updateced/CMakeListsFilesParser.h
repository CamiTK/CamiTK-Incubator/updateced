/*****************************************************************************
* $CAMITK_LICENCE_BEGIN$
*
* CamiTK - Computer Assisted Medical Intervention ToolKit
* (c) 2001-2016 UJF-Grenoble 1, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
*
* Visit http://camitk.imag.fr for more information
*
* This file is part of CamiTK.
*
* CamiTK is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3
* only, as published by the Free Software Foundation.
*
* CamiTK is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License version 3 for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
*
* $CAMITK_LICENCE_END$
****************************************************************************/
#include "CatalogueEditionAPI.h"

#include <QString>
#include <QStringList>
#include <QMap>

/**
* Provides usefull methods to parse a CMake File containing Camitk objects below:
* - camitk_extension_project
* - camitk_extension
* - camitk_application
* - camitk_library
*/
class UPDATECED_API CMakeListsFilesParser
{
public:
	/**
	*/
	enum kindOfExtension
	{
		none,
		camitk_extension_project,
		camitk_action_extension,
		camitk_component_extension,
		camitk_library,
		camitk_application
	};

	/**
	* Build an empty parser
	*/
	CMakeListsFilesParser();

	/**
	* Build a parser for the given file. The fileName is the path of a CMakeLists.txt containing a macro CamiTK. 
	*/
	CMakeListsFilesParser(QString const fileName);
	~CMakeListsFilesParser();

	/**
	* Give the path of the CMakeLists to parse. 
	*/
	void setFileName(QString const fileName);

	QString const getFileName();

	/**
	* parse the given file. Return true if no error has occured. 
	*/
	bool process();

	/**
	* get parameters of extensions.
	*/
	kindOfExtension getKindOfExtension() const;
	/**
	* CamiTK_extension_project parameters
	*/
	QStringList getNeedsCep() const;
	QString getDefaultApplication() const;
	QString getDescription() const;
	QString getContact() const;
	QString getName() const;
	QString getLicense() const;
	bool getEnabled() const;

	/**
	* camitk_extension parameters
	*/
	QStringList getNeedsTool() const; // deprecated
	QStringList getNeedsCepLibraries() const;
	QStringList getNeedsComponentExtension() const;
	QStringList getNeedsActionExtension() const;
	QStringList getIncludeDirectories() const;
	QStringList getExternalLibraries() const;
	QStringList getHeadersToInstall() const;
	QStringList getDefines() const;
	QStringList getCxxFlags() const;
	QStringList getExternalSources() const;
	QString getTargetName() const;
	QString getCepName() const;
	// getDescription()
	QString getTestApplication() const;
	QString getExtraTranslateLanguage() const;
	QStringList getTestFiles() const;
	bool getNeedsQtModules() const;
	bool getActionExtension() const;
	bool getComponentExtension() const;
	bool getDisabled() const;
	bool getNeedsXercesc() const;	
	bool getNeedsITK() const;
	bool getNeedsLibXML2() const;
	bool getNeedsXSD() const;
	bool getNeedsQtXML() const;
	bool getNeedsOpenCV() const;
	bool getNeedsIGSTK() const;
	bool getInstallAllHeaders() const;	
	bool getNeedsGDCM() const;
	bool getDisableTestLevel1() const;
	bool getDisableTestLevel2() const;
	bool getDisableTestLevel3() const;
	
	/**
	* camitk_library parameters
	*/
	QStringList getSources() const;
	// getNeedsCepLibraries()
	// getExternalLibraries()
	// getIncludeDirectories()
	QString getLibName() const;
	//getDefines()
	QStringList getLinkDirectories() const;
	// getHeadersToInstall()
	// getCepName()
	// getDescription()
	// getExtraTranslateLanguage()
	// getCxxFlags()
	bool getShared() const;
	bool getStatic() const;
	// getNeedsITK()
	// getNeedsLibXML2()
	// getNeedsXercesc()
	// getNeedsXSD()
	// getNeedsQtModules()
	bool getPublic();

	/**
	* camitk_application parameters
	*/
	// getNeedsCepLibraries()
	// getNeedsTool()
	// getNeedsComponentExtension()
	// getNeedsActionExtension()
	// getDefines()
	// getCxxFlags()
	QStringList getAdditionalSources() const;
	// getCepName()
	// getDescription()
	// getExternalLibraries()
	// getIncludeDirectories()
	// getExtraTranslateLanguage()
	// getDisabled()
	// getNeedsQtModules()
	// getNeedsITK()
	// getQtXML()
	// getNeedsXSD()
	// getNeedsXercesc()
	bool getNeedsPython() const;

protected:
	/**
	* Extract from the string input the string searchValue that is a camitk_macro (like "camitk_extension_project"; "camitk_extension"; "camitk_application"; "camitk_library") until the closing bracket.
	* It return a one-line QString.
	*/
	QString extractFromString(QString const input, QString const searchValue) const;

	/**
	* extract parameters from the input QString, and return a QMap that contains the name of the parameter and its value for each line.
	*/
	QMap<QString, QString> extractParameters(QString const input) const;

	/**
	* This method build the parameter list that can be find by the parser. 
	* These parameters are those from camiTK_Extension, camiTK_Extension_Project, camitk_library and camitk_application
	* TODO: Update this method if you change the descripion of these camitk extensions.
	*/
	void buildParameterList();



private:
	QString mFileName; 
	kindOfExtension mKind;

	QStringList mParameterList;

	QMap<QString, QString> mParametersMap;
};