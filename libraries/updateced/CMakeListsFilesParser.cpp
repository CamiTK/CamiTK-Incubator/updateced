/*****************************************************************************
* $CAMITK_LICENCE_BEGIN$
*
* CamiTK - Computer Assisted Medical Intervention ToolKit
* (c) 2001-2016 UJF-Grenoble 1, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
*
* Visit http://camitk.imag.fr for more information
*
* This file is part of CamiTK.
*
* CamiTK is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3
* only, as published by the Free Software Foundation.
*
* CamiTK is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License version 3 for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
*
* $CAMITK_LICENCE_END$
****************************************************************************/

#include "CMakeListsFilesParser.h"

#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <QRegularExpression>

#include <QDebug>


CMakeListsFilesParser::CMakeListsFilesParser()
{
	mFileName = "";
	mKind = none;
	mParametersMap.clear();
	mParameterList.clear();
}

CMakeListsFilesParser::CMakeListsFilesParser(QString const fileName)
{
	mFileName = "";
	setFileName(fileName);
	mKind = none;
	mParametersMap.clear();
	mParameterList.clear();

	buildParameterList();
}

CMakeListsFilesParser::~CMakeListsFilesParser()
{
	mParametersMap.clear();
	mParameterList.clear();

	buildParameterList();
}

void CMakeListsFilesParser::buildParameterList()
{
	// Split before each known word
	QString sCEP = "\\b(NEEDS_CEP)\\b|\\b(DEFAULT_APPLICATION)\\b|\\b(DESCRIPTION)\\b|\\b(CONTACT)\\b|\\b(NAME)\\b|\\b(LICENSE)\\b|\\b(ENABLED)\\b";
	QString sExt = "\\b(NEEDS_TOOL)\\b|\\b(NEEDS_CEP_LIBRARIES)\\b|\\b(NEEDS_COMPONENT_EXTENSION)\\b|\\b(NEEDS_ACTION_EXTENSION)\\b|\\b(INCLUDE_DIRECTORIES)\\b|\\b(EXTERNAL_LIBRARIES)\\b|\\b(HEADERS_TO_INSTALL)\\b|\\b(DEFINES)\\b|\\b(CXX_FLAGS)\\b|\\b(EXTERNAL_SOURCES)\\b|\\b(TARGET_NAME)\\b|\\b(CEP_NAME)\\b|\\b(DESCRIPTION)\\b|\\b(TEST_APPLICATION)\\b|\\b(EXTRA_TRANSLATE_LANGUAGE)\\b|\\b(TEST_FILES)\\b|\\b(NEEDS_QT_MODULES)\\b|\\b(ACTION_EXTENSION)\\b|\\b(COMPONENT_EXTENSION)\\b|\\b(DISABLED)\\b|\\b(NEEDS_XERCESC)\\b|\\b(NEEDS_ITK)\\b|\\b(NEEDS_LIBXML2)\\b|\\b(NEEDS_XSD)\\b|\\b(NEEDS_QTXML)\\b|\\b(NEEDS_OPENCV)\\b|\\b(NEEDS_IGSTK)\\b|\\b(INSTALL_ALL_HEADERS)\\b|\\b(NEEDS_GDCM)\\b|\\b(DISABLE_TESTLEVEL1)\\b|\\b(DISABLE_TESTLEVEL2)\\b|\\b(DISABLE_TESTLEVEL3)\\b";
	QString sLib = "\\b(SOURCES)\\b|\\b(NEEDS_CEP_LIBRARIES)\\b|\\b(EXTERNAL_LIBRARIES)\\b|\\b(INCLUDE_DIRECTORIES)\\b|\\b(LIBNAME)\\b|\\b(DEFINES)\\b|\\b(LINK_DIRECTORIES)\\b|\\b(HEADERS_TO_INSTALL)\\b|\\b(CEP_NAME)\\b|\\b(DESCRIPTION)\\b|\\b(EXTRA_TRANSLATE_LANGUAGE)\\b|\\b(CXX_FLAGS)\\b|\\b(SHARED)\\b|\\b(STATIC)\\b|\\b(NEEDS_ITK)\\b|\\b(NEEDS_LIBXML2)\\b|\\b(NEEDS_XERCESC)\\b|\\b(NEEDS_XSD)\\b|\\b(NEEDS_QT_MODULES)\\b|\\b(PUBLIC)\\b";
	QString sApp = "\\b(NEEDS_CEP_LIBRARIES)\\b|\\b(NEEDS_TOOL)\\b|\\b(NEEDS_COMPONENT_EXTENSION)\\b|\\b(NEEDS_ACTION_EXTENSION)\\b|\\b(DEFINES)\\b|\\b(CXX_FLAGS)\\b|\\b(ADDITIONAL_SOURCES)\\b|\\b(CEP_NAME)\\b|\\b(DESCRIPTION)\\b|\\b(EXTERNAL_LIBRARIES)\\b|\\b(INCLUDE_DIRECTORIES)\\b|\\b(EXTRA_TRANSLATE_LANGUAGE)\\b|\\b(DISABLED)\\b|\\b(NEEDS_ITK)\\b|\\b(NEEDS_QTXML)\\b|\\b(NEEDS_XSD)\\b|\\b(NEEDS_XERCESC)\\b|\\b(NEEDS_PYTHON)\\b";

	QString sTot = "";
	sTot.append(sCEP);
	sTot.append("|");
	sTot.append(sExt);
	sTot.append("|");
	sTot.append(sLib);
	sTot.append("|");
	sTot.append(sApp);

	mParameterList = sTot.split("|");
	mParameterList.removeDuplicates();

}

void CMakeListsFilesParser::setFileName(QString const fileName)
{
	mFileName = fileName;
}
QString const CMakeListsFilesParser::getFileName()
{
	return mFileName;
}

bool CMakeListsFilesParser::process()
{
	bool res = false;

	QFile file(mFileName);

	if (file.exists())
	{
		if (file.open(QIODevice::ReadOnly | QIODevice::Text))
		{
			QTextStream in(&file);
			QString all = in.readAll();

			// if it is a CMakeLists.txt from directories, escape.
			if (all.contains("get_subdirectories") != true && all.contains("camitk_add_subdirectory") != true)
			{

				QStringList possibleValues;
				possibleValues << "camitk_extension_project" << "camitk_extension" << "camitk_application" << "camitk_library";


				int i = 0;
				bool find = false;
				QString workingString = "";
				while (i < possibleValues.count() && !find)
				{
					workingString = extractFromString(all, possibleValues.at(i));
					if (workingString.isEmpty() != true)
					{
						find = true;
					}
					i++;
				}

				if (workingString.isEmpty() != true)
				{
					i = i - 1;
					switch (i)
					{
					case 0:
						mKind = camitk_extension_project;
						break;
					case 1:
						if (workingString.contains(QRegExp("\\sCOMPONENT_EXTENSION")) || workingString.contains(QRegExp("\\(COMPONENT_EXTENSION")))
						{
							mKind = camitk_component_extension;
						}
						else if (workingString.contains(QRegExp("\\sACTION_EXTENSION")) || workingString.contains(QRegExp("\\(ACTION_EXTENSION")))
						{
							mKind = camitk_action_extension;
						}
						else
						{
							mKind = none;
						}
						break;
					case 2:
						mKind = camitk_application;
						break;
					case 3:
						mKind = camitk_library;
						break;
					default:
						mKind = none;
						break;
					}
					mParametersMap = extractParameters(workingString);
					res = true;
				}
			}
		}
		file.close();
	}
	return res;
}

QString CMakeListsFilesParser::extractFromString(QString const input, QString const searchValue) const
{
	QString res = "";

	QString regEx;
	regEx = "\\b("; 
	regEx.append(searchValue);
	regEx.append(")\\b");
	regEx.append("\\(.*\\)");

	QRegExp rx(regEx);

	int first = input.indexOf(rx);
	if (first >= 0)
	{
		res = input.mid(first, rx.matchedLength()); 
		int ind = res.indexOf(")");
		if (ind >= 0)
		{
			res = res.mid(0, ind+1);
		}
		res = res.simplified();
	}
	if (res.isEmpty() != true)
	{
		qDebug() << "CMakeListsFilesParser::extractFromString() *** res = " << res; 
	}
	return res;
}

QMap<QString, QString> CMakeListsFilesParser::extractParameters(QString const input) const
{
	// result map
	QMap<QString, QString> resMap;

	// Get only parameters
	QRegExp rx1("\\(.*\\)");
	int i = rx1.indexIn(input);
	QString paramStr = input.mid(i + 1, input.count() - i - 2);

	QList<QRegularExpressionMatch> matchedRegExpList;

	foreach (QString str, mParameterList)
	{
		QRegularExpression re(str);
		QRegularExpressionMatch match = re.match(paramStr);
		if (match.hasMatch())
		{
			matchedRegExpList.append(match);
		}
	}


	// sort the list according to the QRegularExpressionMatch::capturedStart index.
	// Use a Sort "jump-down"
	for (i = matchedRegExpList.count() - 1; i > 0; --i)
	{
		for (int j = 0; j < i; j++)
		{
			if (matchedRegExpList.at(i).capturedStart() < matchedRegExpList.at(j).capturedStart())
			{
				matchedRegExpList.swap(i, j);
			}
		}
	}

	if (matchedRegExpList.isEmpty() != true)
	{
		for (i = 0; i < matchedRegExpList.count(); i++)
		{
			QRegularExpressionMatch exp = matchedRegExpList.at(i);
			QString key = exp.captured();
			QString value = "";
			int nbOfDigits = -1;
			if (i < matchedRegExpList.count() - 1)
			{
				nbOfDigits = matchedRegExpList.at(i + 1).capturedStart() - 1 - exp.capturedEnd();
			}

			value = paramStr.mid(exp.capturedEnd() + 1, nbOfDigits);

			resMap.insert(key, value);
		}
	}

	return resMap;
}

CMakeListsFilesParser::kindOfExtension CMakeListsFilesParser::getKindOfExtension() const
{
	return this->mKind;
}

QStringList CMakeListsFilesParser::getNeedsCep() const
{
	QStringList res;
	return res;
}

QString CMakeListsFilesParser::getDefaultApplication() const
{
	QString res = "";
	return res;
}

QString CMakeListsFilesParser::getDescription() const
{
	QString res = "";
	if (mParametersMap.contains("DESCRIPTION"))
	{
		res = mParametersMap.value("DESCRIPTION");
	}
	return res;
}

QString CMakeListsFilesParser::getContact() const
{
	QString res = "";
	if (mParametersMap.contains("CONTACT"))
	{
		res = mParametersMap.value("CONTACT");
	}
	return res;
}

QString CMakeListsFilesParser::getName() const
{
	QString res = "";
	if (mParametersMap.contains("NAME"))
	{
		res = mParametersMap.value("NAME");
	}
	return res;
}

QString CMakeListsFilesParser::getLicense() const
{
	QString res = "";
	if (mParametersMap.contains("LICENSE"))
	{
		res = mParametersMap.value("LICENSE");
	}
	return res;
}
bool CMakeListsFilesParser::getEnabled() const
{
	bool res = false;
	if (mParametersMap.contains("ENABLED"))
	{
		res = true;
	}
	return res;
}

/**
* camitk_extension parameters
*/
QStringList CMakeListsFilesParser::getNeedsTool() const // deprecated
{
	QStringList res;
	if (mParametersMap.contains("NEEDS_TOOL"))
	{
		res = mParametersMap.value("NEEDS_TOOL").split(QRegExp("\\s+"), QString::SkipEmptyParts);
	}
	return res;
}

QStringList CMakeListsFilesParser::getNeedsCepLibraries() const
{
	QStringList res;
	if (mParametersMap.contains("NEEDS_CEP_LIBRARIES"))
	{
		res = mParametersMap.value("NEEDS_CEP_LIBRARIES").split(QRegExp("\\s+"), QString::SkipEmptyParts);
	}
	return res;
}

QStringList CMakeListsFilesParser::getNeedsComponentExtension() const
{
	QStringList res;
	if (mParametersMap.contains("NEEDS_COMPONENT_EXTENSION"))
	{
		res = mParametersMap.value("NEEDS_COMPONENT_EXTENSION").split(QRegExp("\\s+"), QString::SkipEmptyParts);
	}
	return res;
}

QStringList CMakeListsFilesParser::getNeedsActionExtension() const
{
	QStringList res;
	if (mParametersMap.contains("NEEDS_ACTION_EXTENSION"))
	{
		res = mParametersMap.value("NEEDS_ACTION_EXTENSION").split(QRegExp("\\s+"), QString::SkipEmptyParts);
	}
	return res;
}

QStringList CMakeListsFilesParser::getIncludeDirectories() const
{
	QStringList res;
	if (mParametersMap.contains("INCLUDE_DIRECTORIES"))
	{
		res = mParametersMap.value("INCLUDE_DIRECTORIES").split(QRegExp("\\s+"), QString::SkipEmptyParts);
	}
	return res;
}

QStringList CMakeListsFilesParser::getExternalLibraries() const
{
	QStringList res;
	if (mParametersMap.contains("EXTERNAL_LIBRARIES"))
	{
		res = mParametersMap.value("EXTERNAL_LIBRARIES").split(QRegExp("\\s+"), QString::SkipEmptyParts);
	}
	return res;
}

QStringList CMakeListsFilesParser::getHeadersToInstall() const
{
	QStringList res;
	if (mParametersMap.contains("HEADERS_TO_INSTALL"))
	{
		res = mParametersMap.value("HEADERS_TO_INSTALL").split(QRegExp("\\s+"), QString::SkipEmptyParts);
	}
	return res;
}

QStringList CMakeListsFilesParser::getDefines() const
{
	QStringList res;
	if (mParametersMap.contains("DEFINES"))
	{
		res = mParametersMap.value("DEFINES").split(QRegExp("\\s+"), QString::SkipEmptyParts);
	}
	return res;
}

QStringList CMakeListsFilesParser::getCxxFlags() const
{
	QStringList res;
	if (mParametersMap.contains("CXX_FLAGS"))
	{
		res = mParametersMap.value("CXX_FLAGS").split(QRegExp("\\s+"), QString::SkipEmptyParts);
	}
	return res;
}

QStringList CMakeListsFilesParser::getExternalSources() const
{
	QStringList res;
	if (mParametersMap.contains("EXTERNAL_SOURCES"))
	{
		res = mParametersMap.value("EXTERNAL_SOURCES").split(QRegExp("\\s+"), QString::SkipEmptyParts);
	}
	return res;
}

QString CMakeListsFilesParser::getTargetName() const
{
	QString res = "";
	if (mParametersMap.contains("TARGET_NAME"))
	{
		res = mParametersMap.value("TARGET_NAME");
	}
	return res;
}

QString CMakeListsFilesParser::getCepName() const
{
	QString res = "";
	if (mParametersMap.contains("CEP_NAME"))
	{
		res = mParametersMap.value("CEP_NAME");
	}
	return res;
}

// getDescription()
QString CMakeListsFilesParser::getTestApplication() const
{
	QString res = "";
	if (mParametersMap.contains("TEST_APPLICATION"))
	{
		res = mParametersMap.value("TEST_APPLICATION");
	}
	return res;
}

QString CMakeListsFilesParser::getExtraTranslateLanguage() const
{
	QString res = "";
	if (mParametersMap.contains("EXTRA_TRANSLATE_LANGUAGE"))
	{
		res = mParametersMap.value("EXTRA_TRANSLATE_LANGUAGE");
	}
	return res;
}

QStringList CMakeListsFilesParser::getTestFiles() const
{
	QStringList res;
	if (mParametersMap.contains("TEST_FILES"))
	{
		res = mParametersMap.value("TEST_FILES").split(QRegExp("\\s+"), QString::SkipEmptyParts);
	}
	return res;
}

bool CMakeListsFilesParser::getNeedsQtModules() const
{
	bool res = false;
	if (mParametersMap.contains("NEEDS_QT_MODULES"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getActionExtension() const
{
	bool res = false;
	if (mParametersMap.contains("ACTION_EXTENSION"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getComponentExtension() const
{
	bool res = false;
	if (mParametersMap.contains("COMPONENT_EXTENSION"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getDisabled() const
{
	bool res = false;
	if (mParametersMap.contains("DISABLED"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getNeedsXercesc() const
{
	bool res = false;
	if (mParametersMap.contains("NEEDS_XERCESC"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getNeedsITK() const
{
	bool res = false;
	if (mParametersMap.contains("NEEDS_ITK"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getNeedsLibXML2() const
{
	bool res = false;
	if (mParametersMap.contains("NEEDS_LIBXML2"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getNeedsXSD() const
{
	bool res = false;
	if (mParametersMap.contains("NEEDS_XSD"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getNeedsQtXML() const
{
	bool res = false;
	if (mParametersMap.contains("NEEDS_QTXML"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getNeedsOpenCV() const
{
	bool res = false;
	if (mParametersMap.contains("NEEDS_OPENCV"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getNeedsIGSTK() const
{
	bool res = false;
	if (mParametersMap.contains("NEEDS_IGSTK"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getInstallAllHeaders() const
{
	bool res = false;
	if (mParametersMap.contains("INSTALL_ALL_HEADERS"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getNeedsGDCM() const
{
	bool res = false;
	if (mParametersMap.contains("NEEDS_GDCM"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getDisableTestLevel1() const
{
	bool res = false;
	if (mParametersMap.contains("DISABLE_TESTLEVEL1"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getDisableTestLevel2() const
{
	bool res = false;
	if (mParametersMap.contains("DISABLE_TESTLEVEL2"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getDisableTestLevel3() const
{
	bool res = false;
	if (mParametersMap.contains("DISABLE_TESTLEVEL3"))
	{
		res = true;
	}
	return res;
}


/**
* camitk_library parameters
*/
QStringList CMakeListsFilesParser::getSources() const
{
	QStringList res;
	if (mParametersMap.contains("SOURCES"))
	{
		res = mParametersMap.value("SOURCES").split(QRegExp("\\s+"), QString::SkipEmptyParts);
	}
	return res;
}

// getNeedsCepLibraries()
// getExternalLibraries()
// getIncludeDirectories()
QString CMakeListsFilesParser::getLibName() const
{
	QString res = "";
	if (mParametersMap.contains("LIBNAME"))
	{
		res = mParametersMap.value("LIBNAME");
	}
	return res;
}

//getDefines()
QStringList CMakeListsFilesParser::getLinkDirectories() const
{
	QStringList res;
	if (mParametersMap.contains("LINK_DIRECTORIES"))
	{
		res = mParametersMap.value("LINK_DIRECTORIES").split(QRegExp("\\s+"), QString::SkipEmptyParts);
	}
	return res;
}

// getHeadersToInstall()
// getCepName()
// getDescription()
// getExtraTranslateLanguage()
// getCxxFlags()
bool CMakeListsFilesParser::getShared() const
{
	bool res = false;
	if (mParametersMap.contains("SHARED"))
	{
		res = true;
	}
	return res;
}

bool CMakeListsFilesParser::getStatic() const
{
	bool res = false;
	if (mParametersMap.contains("STATIC"))
	{
		res = true;
	}
	return res;
}

// getNeedsITK()
// getNeedsLibXML2()
// getNeedsXercesc()
// getNeedsXSD()
// getNeedsQtModules()
bool CMakeListsFilesParser::getPublic()
{
	bool res = false;
	if (mParametersMap.contains("PUBLIC"))
	{
		res = true;
	}
	return res;
}


/**
* camitk_application parameters
*/
// getNeedsCepLibraries()
// getNeedsTool()
// getNeedsComponentExtension()
// getNeedsActionExtension()
// getDefines()
// getCxxFlags()
QStringList CMakeListsFilesParser::getAdditionalSources() const
{
	QStringList res;
	if (mParametersMap.contains("ADDITIONAL_SOURCES"))
	{
		res = mParametersMap.value("ADDITIONAL_SOURCES").split(QRegExp("\\s+"), QString::SkipEmptyParts);
	}
	return res;
}

// getCepName()
// getDescription()
// getExternalLibraries()
// getIncludeDirectories()
// getExtraTranslateLanguage()
// getDisabled()
// getNeedsQtModules()
// getNeedsITK()
// getQtXML()
// getNeedsXSD()
// getNeedsXercesc()
bool CMakeListsFilesParser::getNeedsPython() const
{
	bool res = false;
	if (mParametersMap.contains("NEEDS_PYTHON"))
	{
		res = true;
	}
	return res;
}
