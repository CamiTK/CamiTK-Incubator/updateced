/*****************************************************************************
* $CAMITK_LICENCE_BEGIN$
*
* CamiTK - Computer Assisted Medical Intervention ToolKit
* (c) 2001-2016 UJF-Grenoble 1, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
*
* Visit http://camitk.imag.fr for more information
*
* This file is part of CamiTK.
*
* CamiTK is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3
* only, as published by the Free Software Foundation.
*
* CamiTK is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License version 3 for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
*
* $CAMITK_LICENCE_END$
****************************************************************************/
#include "CEPInspection.h"
#include "CMakeListsFilesParser.h"

// camitk2cepcoreschema dependencies
#include <CepC2CepS.h>
#include <ActionExtC2ActionExtS.h>
#include <ComponentExtC2ComponentExtS.h>
#include <LibraryC2LibraryS.h>
#include <ApplicationC2ApplicationS.h>
#include <DependencyC2DependencyS.h>
#include <ActionC2ActionS.h>
#include <ParameterC2ParameterS.h>
#include <ComponentC2ComponentS.h>

// CamiTK dependencies
#include <ActionExtension.h>
#include <ComponentExtension.h>
#include <Component.h>
#include <Action.h>
#include <Property.h>
#include <ExtensionManager.h>

// CepGenerator dependencies
#include "CepGenerator.h"

// Qt Dependencies
#include <QFileInfo>
#include <QDir>

#include <QDebug>

CEPInspection::CEPInspection()
{
	init();
}

CEPInspection::CEPInspection(QString srcPath, QString buildPath)
{
	init();
	setPathSrc(srcPath);
	setPathBuild(buildPath);
}

CEPInspection::~CEPInspection()
{
	foreach (CMakeListsFilesParser* parser, mCorrespondancesMap.keys())
	{
		delete parser;
	}
	mCorrespondancesMap.clear();
	delete mConvertor;
	mConvertor = NULL;
}

void CEPInspection::init()
{
	mConvertor = NULL;
	mConvertor = new CepC2CepS();
	mPathBuild = "";
	mPathSrc = "";
	mListCEPContentFile.clear();
	mListCMakeFile.clear();
	mListDynamicLibrariesFile.clear();
	mCorrespondancesMap.clear();
}

bool CEPInspection::findFileExistingCEPContent()
{
	return findFileAndFillList("CEPContent.xml", mPathSrc, mListCEPContentFile);
}

bool CEPInspection::findFileCMakeLists()
{
	return findFileAndFillList("CMakeLists.txt", mPathSrc, mListCMakeFile);
}

bool CEPInspection::findFileDynamicLibraries()
{
	return findFileAndFillList(".dll", mPathBuild, mListDynamicLibrariesFile);
}

bool CEPInspection::findFileAndFillList(QString fileName, QString dirPath, QStringList &list)
{
	bool res = false;
	if (list.isEmpty())
	{
		findFileInDirectory(fileName, dirPath, list);
	}
	if (list.isEmpty() != true)
	{
		res = true;
	}
	return res;
}

void CEPInspection::findFileInDirectory(QString fileName, QString dirPath, QStringList & filePathList)
{
	QDir dir(dirPath);
	QFileInfoList fileList = dir.entryInfoList();
	foreach(QFileInfo fileInfo, fileList)
	{
		if (fileInfo.fileName().contains(fileName, Qt::CaseInsensitive))
		{
			filePathList.append(fileInfo.absoluteFilePath());
		}
		else if (fileInfo.isDir() && (fileInfo.fileName() != "." && fileInfo.fileName() != ".."))
		{
			findFileInDirectory(fileName, fileInfo.absoluteFilePath(), filePathList);
		}
	}
}

bool CEPInspection::loadExistingCepContentFile()
{
	bool res = false;
	if (this->mListCEPContentFile.isEmpty() != true)
	{
		CepGenerator* cepGenerator = new CepGenerator();
		cepGenerator->setXmlFileName(mListCEPContentFile.at(0));
		cepGenerator->createDomTree();

		// release the auto_ptr to avoid conflict when destroy the object mConvertor
		mConvertor->setCepCoreSchemaCep(cepGenerator->getDomCep().release());
		res = true;
		delete cepGenerator;

		qDebug() << "Cep Name: " << mConvertor->getName();

	}
	return res;
}

CMakeListsFilesParser* CEPInspection::parseCmakeListFile(QString const fileName)
{
	CMakeListsFilesParser* parser = new CMakeListsFilesParser(fileName);
	parser->process();
	qDebug() << "kind: " << parser->getKindOfExtension();
	return parser;
}

void CEPInspection::updateCamiTKExtensionProject(CepC2CepS* cep, CMakeListsFilesParser* parser)
{
	if (cep != NULL && parser != NULL)
	{
		if (parser->getKindOfExtension() == CMakeListsFilesParser::camitk_extension_project)
		{
			if (parser->getName().isEmpty() != true)
			{
				cep->setName(parser->getName());
			}
			if (parser->getContact().isEmpty() != true)
			{
				cep->setContact(parser->getContact().split(","));
			}
			if (parser->getLicense().isEmpty() != true)
			{
				cep->setLicence(parser->getLicense());
			}
			if (parser->getDescription().isEmpty() != true)
			{
				cep->setDescription(parser->getDescription());
			}
		}
		else
		{
			// is it usefull to update with only an xml file ? That come from where ?
		}
	}
}

void CEPInspection::updateActionExtension(ActionExtC2ActionExtS* actExt, CMakeListsFilesParser* parser, camitk::ActionExtension* camitkActExt)
{
	if (actExt != NULL && parser != NULL)
	{
		bool isLibDefined = false;
		if (camitkActExt != NULL)
		{
			isLibDefined = true;
		}

		if (isLibDefined)
		{
			QString extName = camitkActExt->getName();
			if (extName.isEmpty())
			{

				QFileInfo fileInfo(camitkActExt->getLocation());
				extName = fileInfo.dir().dirName();
			}
			actExt->setName(extName);
			actExt->setDescription(camitkActExt->getDescription());
		}
		else
		{
			if (parser->getTargetName().isEmpty() != true)
			{
				actExt->setName(parser->getTargetName());
			}
			else
			{
				QFileInfo fileIn(parser->getFileName());
				actExt->setName(fileIn.dir().dirName());
			}
			if (parser->getDescription().isEmpty() != true)
			{
				actExt->setDescription(parser->getDescription());
			}
		}

		// Add dependencies
		QList<DependencyC2DependencyS*> depList = createListDependenciesFromCMakeFile(parser);
		// Remove all existing dependencies
		actExt->removeAllDependencies();
		foreach(DependencyC2DependencyS* dep, depList)
		{
			actExt->addDependency(dep);
		}

		if (isLibDefined)
		{
			// Remove all existing actions. 
			actExt->removeAllActions();
			// Then get actions from library.
			QList<ActionC2ActionS*> actList = createListActionsFromBuildLibrary(camitkActExt);
			foreach(ActionC2ActionS* act, actList)
			{
				actExt->addAction(act);
			}

		}
	}
}

void CEPInspection::updateComponentExtension(ComponentExtC2ComponentExtS* compExt, CMakeListsFilesParser* parser, camitk::ComponentExtension* camitkCompExt)
{
	qDebug() << "CEPInspection::updateComponentExtension() -- Inside";
	if (compExt != NULL && parser != NULL)
	{
		bool isLibDefined = false;
		if (camitkCompExt != NULL)
		{
			isLibDefined = true;
		}

		if (isLibDefined)
		{
			QString extName = camitkCompExt->getName();
			if (extName.isEmpty())
			{

				QFileInfo fileInfo(camitkCompExt->getLocation());
				extName = fileInfo.dir().dirName();
			}
			compExt->setName(extName);

			compExt->setDescription(camitkCompExt->getDescription());
		}
		else
		{
			if (parser->getTargetName().isEmpty() != true)
			{
				compExt->setName(parser->getTargetName());
			}
			else
			{
				QFileInfo fileIn(parser->getFileName());
				compExt->setName(fileIn.dir().dirName());
			}
			if (parser->getDescription().isEmpty() != true)
			{
				compExt->setDescription(parser->getDescription());
			}
		}
		// Add dependencies
		QList<DependencyC2DependencyS*> depList = createListDependenciesFromCMakeFile(parser);
		// Remove all existing dependencies
		compExt->removeAllDependencies();
		foreach(DependencyC2DependencyS* dep, depList)
		{
			compExt->addDependency(dep);
		}
		if (isLibDefined)
		{
			// Remove all existing components. 
			compExt->removeAllComponents();
			// Then get actions from library.
			QList<ComponentC2ComponentS*> compList = createListComponentsFromBuildLibrary(camitkCompExt);
			qDebug() << "CEPInspection::updateComponentExtension() -- CEPInspection::updateComponentExtension compList.count(): " << compList.count();
			foreach(ComponentC2ComponentS* comp, compList)
			{
				compExt->addComponent(comp);
			}

		}
	}
}

void CEPInspection::updateLibrary(LibraryC2LibraryS* lib, CMakeListsFilesParser* parser)
{
	if (lib != NULL && parser != NULL)
	{
		if (parser->getLibName().isEmpty() != true)
		{
			lib->setName(parser->getLibName());
		}
		else
		{
			QFileInfo fileIn(parser->getFileName());
			lib->setName(fileIn.dir().dirName());
		}
		if (parser->getDescription().isEmpty() != true)
		{
			lib->setDescription(parser->getDescription());
		}
		// Add dependencies
		QList<DependencyC2DependencyS*> depList = createListDependenciesFromCMakeFile(parser);
		// Remove all existing dependencies
		lib->removeAllDependencies();
		foreach(DependencyC2DependencyS* dep, depList)
		{
			lib->addDependency(dep);
		}
	}
}

void CEPInspection::updateApplication(ApplicationC2ApplicationS* app, CMakeListsFilesParser* parser)
{
	if (app != NULL && parser != NULL)
	{
		// There is no name for application (we must get the name of the directory, and it is not done at this time. TODO !if (parser->get)
	
		QFileInfo fileIn(parser->getFileName());
		app->setName(fileIn.dir().dirName());
	
		if (parser->getDescription().isEmpty() != true)
		{
			app->setDescription(parser->getDescription());
		}

		//// Add dependencies: There is no dependencies defined at this time for applications in XML schema ! TODO ?
		//QList<DependencyC2DependencyS*> depList = createListDependenciesFromCMakeFile(parser);
		//// Remove all existing dependencies
		//app->removeAllDependencies();
		//foreach(DependencyC2DependencyS* dep, depList)
		//{
		//	app->addDependency(dep);
		//}
	}
}

QList<DependencyC2DependencyS*> CEPInspection::createListDependenciesFromCMakeFile(CMakeListsFilesParser* parser)
{
	QList<DependencyC2DependencyS*> listOfDependencies;
	if (parser != NULL)
	{
		if (parser->getNeedsCep().isEmpty() != true)
		{
			foreach(QString str, parser->getNeedsCep())
			{
				DependencyC2DependencyS* dep = new DependencyC2DependencyS();
				dep->setType("cepLibrary");
				dep->setName(str);
				listOfDependencies.append(dep);
			}
		}

		if (parser->getNeedsCepLibraries().isEmpty() != true)
		{
			foreach(QString str, parser->getNeedsCepLibraries())
			{
				DependencyC2DependencyS* dep = new DependencyC2DependencyS();
				dep->setType("cepLibrary");
				dep->setName(str);
				listOfDependencies.append(dep);
			}
		}
		
		if (parser->getNeedsComponentExtension().isEmpty() != true)
		{
			foreach(QString str, parser->getNeedsComponentExtension())
			{
				DependencyC2DependencyS* dep = new DependencyC2DependencyS();
				dep->setType("component");
				dep->setName(str);
				listOfDependencies.append(dep);
			}
		}

		if (parser->getNeedsActionExtension().isEmpty() != true)
		{
			foreach(QString str, parser->getNeedsActionExtension())
			{
				DependencyC2DependencyS* dep = new DependencyC2DependencyS();
				dep->setType("action");
				dep->setName(str);
				listOfDependencies.append(dep);
			}
		}

		if (parser->getExternalLibraries().isEmpty() != true)
		{
			foreach(QString str, parser->getExternalLibraries())
			{
				DependencyC2DependencyS* dep = new DependencyC2DependencyS();
				dep->setType("library");
				dep->setName(str);
				listOfDependencies.append(dep);
			}
		}

		if (parser->getNeedsQtModules())
		{
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			dep->setType("library");
			dep->setName("qt_modules");
			listOfDependencies.append(dep);
		}

		if (parser->getNeedsXercesc())
		{
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			dep->setType("library");
			dep->setName("Xercesc");
			listOfDependencies.append(dep);
		}

		if (parser->getNeedsITK())
		{
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			dep->setType("library");
			dep->setName("ITK");
			listOfDependencies.append(dep);
		}

		if (parser->getNeedsLibXML2())
		{
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			dep->setType("library");
			dep->setName("LibXML2");
			listOfDependencies.append(dep);
		}

		if (parser->getNeedsXSD())
		{
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			dep->setType("library");
			dep->setName("XSD");
			listOfDependencies.append(dep);
		}

		if (parser->getNeedsQtXML())
		{
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			dep->setType("library");
			dep->setName("QtXML");
			listOfDependencies.append(dep);
		}

		if (parser->getNeedsOpenCV())
		{
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			dep->setType("library");
			dep->setName("OpenCV");
			listOfDependencies.append(dep);
		}

		if (parser->getNeedsIGSTK())
		{
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			dep->setType("library");
			dep->setName("IGSTK");
			listOfDependencies.append(dep);
		}

		if (parser->getNeedsGDCM())
		{
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			dep->setType("library");
			dep->setName("GDCM");
			listOfDependencies.append(dep);
		}

		if (parser->getNeedsPython())
		{
			DependencyC2DependencyS* dep = new DependencyC2DependencyS();
			dep->setType("library");
			dep->setName("Python");
			listOfDependencies.append(dep);
		}
	}

	return listOfDependencies;
}

QList<ActionC2ActionS*> CEPInspection::createListActionsFromBuildLibrary(camitk::ActionExtension* camitkActExt)
{
	QList<ActionC2ActionS*> listOfActions;
	if (camitkActExt)
	{
		camitk::ActionList actList = camitkActExt->getActions();
		foreach(camitk::Action* act, actList)
		{
			if (act->getName().isEmpty() != true)
			{
				ActionC2ActionS* action = new ActionC2ActionS();
				action->setName(act->getName());
				action->setDescription(act->getDescription());
				action->setComponent(act->getComponent());
				action->setFamily(act->getFamily());
				foreach(QString str, act->getTag())
				{
					action->addTag(str);
				}

				QList<QByteArray> propertyNames = act->dynamicPropertyNames();

				foreach(QString name, propertyNames)
				{
					ParameterC2ParameterS* param = createParameterFromCamiTKProperty(act->getProperty(name));

					if (param)
					{
						action->addParameter(param);
					}
				}
				listOfActions.append(action);
			}
		}
	}

	return listOfActions;
}

QList<ComponentC2ComponentS*> CEPInspection::createListComponentsFromBuildLibrary(camitk::ComponentExtension* camitkCompExt)
{
	QList<ComponentC2ComponentS*> listOfComponents;
	if (camitkCompExt)
	{
		// Find test data
		QString extLocation = camitkCompExt->getLocation();
		QFileInfo fileInfo(extLocation);
		QDir dir(fileInfo.absoluteDir());
		dir.cdUp();
		dir.cdUp();
		dir.cdUp();
		dir.cd("share/camitk-4.0/testdata");
		QStringList testDataFileList;
		QFileInfoList list = dir.entryInfoList(QDir::NoFilter);
		foreach(QFileInfo var, list)
		{
			if (var.isDir() != true)
			{
				testDataFileList.append(var.absoluteFilePath());
			}
		}

		foreach(QString str, testDataFileList)
		{
			try
			{
				qDebug() << "CEPInspection::createListComponentsFromBuildLibrary () -- try to open file: " << str;
				camitk::Component* component = camitkCompExt->open(str);
				if (component)
				{
					ComponentC2ComponentS* comp = new ComponentC2ComponentS();
					comp->setName(component->metaObject()->className());
					// TODO: Warning! This is not the description ! Do it xwith the CEPContent.xml or leave it empty (but I think this is not possible !)
					comp->setDescription(component->getLabel());

					camitk::Component::Representation rep = component->getRepresentation();
					QString representation = "";
					switch (rep)
					{
                        case camitk::Component::GEOMETRY:
						representation = "Mesh";
						break;
					case camitk::Component::SLICE:
						representation = "Image";
						break;
					case camitk::Component::NO_REPRESENTATION:
						// To get the representation Image if we have an instance of ImageComponent. 
						if (component->isInstanceOf("ImageComponent"))
						{
							representation = "Image";
						}
						else
						{
							representation = "None";
						}
						break;
					default:
						representation = "None";
						break;
					}
					comp->setRepresentation(representation);

					QFileInfo fiInfo(str);
					comp->setFileSuffix(fiInfo.completeSuffix());

					QList<QByteArray> propertyNames = component->dynamicPropertyNames();

					foreach(QString name, propertyNames)
					{
						ParameterC2ParameterS* param = createParameterFromCamiTKProperty(component->getProperty(name));

						if (param)
						{
							comp->addProperty(param);
						}
					}
					listOfComponents.append(comp);
				}
				
			}
			catch (std::exception e)
			{
				qDebug() << "CEPInspection::createListComponentsFromBuildLibrary() -- Failed to open file " << e.what() ;
			}
			
		}
	}
	return listOfComponents;
}

ParameterC2ParameterS* CEPInspection::createParameterFromCamiTKProperty(camitk::Property* camitkProp)
{
	ParameterC2ParameterS* param = NULL;
	if (camitkProp)
	{
		param = new ParameterC2ParameterS();

		param->setName(camitkProp->getName());
		param->setDescription(camitkProp->getDescription());
		param->setDefaultValue(camitkProp->getInitialValue().toString());
		param->setType(camitkProp->getGroupName());
		param->setEditable(!camitkProp->getReadOnly());
	}
	return param;
}

void CEPInspection::buildCorrespondanciesMap()
{
	if (mListCMakeFile.isEmpty() != true)
	{
		foreach(QString var, mListCMakeFile)
		{
			QFileInfo fileInfo(var);

			CMakeListsFilesParser* tmpParser = new CMakeListsFilesParser(var);
			tmpParser->process();

			QFileInfo* result = new QFileInfo();

			if (tmpParser->getKindOfExtension() == CMakeListsFilesParser::camitk_action_extension ||
				tmpParser->getKindOfExtension() == CMakeListsFilesParser::camitk_component_extension ||
				tmpParser->getKindOfExtension() == CMakeListsFilesParser::camitk_library ||
				tmpParser->getKindOfExtension() == CMakeListsFilesParser::camitk_application)
			{
				

				// don't try to get binary code for libraries or applications, because the build code is not inspected. 
				if (tmpParser->getKindOfExtension() == CMakeListsFilesParser::camitk_action_extension ||
					tmpParser->getKindOfExtension() == CMakeListsFilesParser::camitk_component_extension)
				{
					// try to find dynamic library
					QString extName = tmpParser->getName();
					qDebug() << "CEPInspection::buildCorrespondanciesMap() tmpParser->getName(): " << extName;
					if (extName.isEmpty())
					{
						extName = fileInfo.dir().dirName();
					}
					qDebug() << "CEPInspection::buildCorrespondanciesMap() (different of the previous if the name of the extension is not the name of the directory: " << extName;

					QString regEx = extName;
					regEx.append(".|");
					regEx.append(extName);
					regEx.append("-debug");

					

					// if we have built libraries
					if (mListDynamicLibrariesFile.isEmpty() != true)
					{
						QFileInfo fileDebug;
						QFileInfo fileRelease;

						// Find extName in the list of dll
						QRegExp rx(regEx);
						foreach(QString str, mListDynamicLibrariesFile)
						{
							if (rx.indexIn(str) > -1)
							{
								if (fileDebug.exists())
								{
									fileRelease.setFile(str);
								}
								else
								{
									fileDebug.setFile(str);
								}
							}
						}
						// if a library is found 
						if (fileDebug.exists())
						{
							// if there are a debug and a release libraries...
							if (fileRelease.exists())
							{
								// We choose the most recent library
								if (fileDebug.lastModified() >= fileRelease.lastModified())
								{
									result->setFile(fileDebug.absoluteFilePath());
								}
								else
								{
									result->setFile(fileRelease.absoluteFilePath());
								}
							}
							// else we choose only one library
							else
							{
								result->setFile(fileDebug.absoluteFilePath());
							}
						}
						else
						{
							// Suppose that the QFile is empty
							qDebug() << "We are in the else: no dll has been found";
						}
					}
					qDebug() << " Just before insert in the map: "<< result->absoluteFilePath();
					mCorrespondancesMap.insert(tmpParser, *result);
				}
				else
				{
					// get the parser in the map, with no link to binaries (we had an application or a library)
					mCorrespondancesMap.insert(tmpParser, *result);
				}
			}
			else if (tmpParser->getKindOfExtension() == CMakeListsFilesParser::camitk_extension_project)
			{
				// get the parser in the map, with no link to binaries (we have an extension_project)
				mCorrespondancesMap.insert(tmpParser, *result);
			}
		}
		// Print results of mCorrespondancesMap
		foreach(CMakeListsFilesParser* parser, mCorrespondancesMap.keys())
		{
			qDebug() << "Map: *** parser: " << parser->getFileName() << " *** DLL: " << mCorrespondancesMap.value(parser).absoluteFilePath();
		}
	}
}

bool CEPInspection::loadExistingDynamicLibraries(QFileInfo file)
{
	qDebug() << "CEPInspection::loadExistingDynamicLibraries : file.absolutePath(): " << file.absoluteFilePath();
	bool res = false;
	if (file.exists())
	{
		if (camitk::ExtensionManager::loadExtension(camitk::ExtensionManager::ACTION, file.absoluteFilePath()))
		{
			res = true;
		}
		else if (camitk::ExtensionManager::loadExtension(camitk::ExtensionManager::COMPONENT, file.absoluteFilePath()))
		{
			res = true;
		}
	}
	return res;
}

bool CEPInspection::process()
{
	bool res = false;
	// Call BuildMap
	if (this->mCorrespondancesMap.isEmpty())
	{
		findFileCMakeLists();
		findFileDynamicLibraries();
		buildCorrespondanciesMap();
	}

	if (this->mConvertor != NULL)
	{
		QList<CMakeListsFilesParser*> parserList = mCorrespondancesMap.keys();
		foreach(CMakeListsFilesParser* parser, parserList)
		{
			if (parser->getKindOfExtension() == CMakeListsFilesParser::camitk_extension_project)
			{
				updateCamiTKExtensionProject(mConvertor, parser);
				res = true;
			}

			else if (parser->getKindOfExtension() == CMakeListsFilesParser::camitk_action_extension)
			{
				ActionExtC2ActionExtS* actExt = new ActionExtC2ActionExtS();
				
				bool resLoad = loadExistingDynamicLibraries(mCorrespondancesMap.value(parser));
				if (resLoad)
				{
					QList<camitk::ActionExtension*> listActExt = camitk::ExtensionManager::getActionExtensionsList();
					camitk::ActionExtension* tmp = NULL;
					qDebug() << "CEPInspection::process() -- camitk::ExtensionManager::getActionExtensionsList().count()" << listActExt.count();
					if (listActExt.count() > 0)
					{
						tmp = listActExt.at(0);
						qDebug() << "CEPInspection::process() -- camitk::ExtensionManager::getActionExtensionsList().at(0) Number of actions: " << tmp->getActions().count();
					}
					else
					{
						qDebug() << "CEPInspection::process() -- ActionExtension is null but the result of load is true ! " << parser->getFileName();
					}
					
					updateActionExtension(actExt, parser, tmp);
				}
				else
				{
					updateActionExtension(actExt, parser);
				}
				mConvertor->addActionExtension(actExt);
				if (resLoad)
				{
					camitk::ExtensionManager::unloadAllActionExtensions();
				}
				res = true;
			}

			else if (parser->getKindOfExtension() == CMakeListsFilesParser::camitk_component_extension)
			{
				ComponentExtC2ComponentExtS* compExt = new ComponentExtC2ComponentExtS();

				bool resLoad = loadExistingDynamicLibraries(mCorrespondancesMap.value(parser));
				if (resLoad)
				{
					QList<camitk::ComponentExtension*> listCompExt = camitk::ExtensionManager::getComponentExtensionsList();

					camitk::ComponentExtension* tmp = NULL;
					if (listCompExt.count() > 0)
					{
						tmp = listCompExt.at(0);
						qDebug() << "CEPInspection::process() -- Name of the extension to be explored " << tmp->getName();
					}
					else
					{
						qDebug() << "CEPInspection::process() -- ComponentExtension is null but the result of load is true ! " << parser->getFileName();
					}
					

					updateComponentExtension(compExt, parser, tmp);
				}
				else
				{
					updateComponentExtension(compExt, parser);
				}
				mConvertor->addComponentExtension(compExt);
				if (resLoad)
				{
					QList<camitk::ComponentExtension*> compList = camitk::ExtensionManager::getComponentExtensionsList();
					if (compList.count() > 0)
					{
						camitk::ComponentExtension* tmp = compList.at(0);
						QStringList fileExtList = tmp->getFileExtensions();
						if (fileExtList.count() > 0)
						{
							camitk::ExtensionManager::unloadComponentExtension(fileExtList.at(0));
						}
					}
				}
				res = true;
			}

			else if (parser->getKindOfExtension() == CMakeListsFilesParser::camitk_library)
			{
				LibraryC2LibraryS* libExt = new LibraryC2LibraryS();
				updateLibrary(libExt, parser);
				mConvertor->addLibrary(libExt);
				res = true;
			}
			else if (parser->getKindOfExtension() == CMakeListsFilesParser::camitk_application)
			{
				ApplicationC2ApplicationS* appExt = new ApplicationC2ApplicationS();
				updateApplication(appExt, parser);
				mConvertor->addApplication(appExt);
				res = true;
			}
		}

		res = writeDomTreeInAFile();
	}
	return res;
}

bool CEPInspection::writeDomTreeInAFile()
{
	bool res = false;
	// TODO: this is a test for now ! Create a result file in the src directory. This file is called SuperCEPContent.xml
	std::auto_ptr<cepcoreschema::Cep> domCepPtr(mConvertor->getCepCoreSchemaCep());
    QFileInfo fileInfo;
    QString fileName;
    fileName = mConvertor->getName();
    fileName.remove("\"");
    fileName = fileName.simplified() + ".ced";
    fileInfo.setFile(getPathSrc(), fileName);
	
	CepGenerator* cepGenerator = new CepGenerator(domCepPtr, getPathSrc());	
	cepGenerator->serializeManifest(fileInfo.absoluteFilePath());

	// release the auto_ptr to avoid conflict when destroy the object mConvertor
	mConvertor->setCepCoreSchemaCep(cepGenerator->getDomCep().release());
	res = true;
	delete cepGenerator;

	return res;
}