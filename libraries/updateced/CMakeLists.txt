# Call CamiTK CMake Macro to define UPDATECED library
gather_headers_and_sources(UPDATECED)

camitk_library( SHARED
		SOURCES ${UPDATECED_HEADERS} ${UPDATECED_SOURCES} 
				NEEDS_XSD
				NEEDS_QT
				NEEDS_CAMITKCORE
                DESCRIPTION "This library contains methods to update the CamiTK Extensions Project description file .ced"

				DEFINES COMPILE_UPDATECED_API

				PUBLIC

                #needed for now, to temporarily solve add_dependency problem
#                 NEEDS_CEP_LIBRARIES camitk2cepcoreschema cepgenerator cepcoreschema # camitkcore
                INCLUDE_DIRECTORIES ${CAMITK_BUILD_INCLUDE_DIR}/libraries/camitk2cepcoreschema ${CAMITK_INCLUDE_DIR}/libraries/camitk2cepcoreschema ${CAMITK_BUILD_INCLUDE_DIR}/libraries/cepgenerator ${CAMITK_INCLUDE_DIR}/libraries/cepgenerator ${CAMITK_BUILD_INCLUDE_DIR}/libraries/cepcoreschema ${CAMITK_INCLUDE_DIR}/libraries/cepcoreschema
                
                )
target_link_libraries(library-updateced camitk2cepcoreschema cepgenerator cepcoreschema)



