/*****************************************************************************
* $CAMITK_LICENCE_BEGIN$
*
* CamiTK - Computer Assisted Medical Intervention ToolKit
* (c) 2001-2016 UJF-Grenoble 1, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
*
* Visit http://camitk.imag.fr for more information
*
* This file is part of CamiTK.
*
* CamiTK is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3
* only, as published by the Free Software Foundation.
*
* CamiTK is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License version 3 for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
*
* $CAMITK_LICENCE_END$
****************************************************************************/
#include "CatalogueEditionAPI.h"


#include <QStringList>
#include <QFileInfo>
#include <QMap>

class CepC2CepS;
class ActionExtC2ActionExtS;
class ComponentExtC2ComponentExtS;
class LibraryC2LibraryS;
class ApplicationC2ApplicationS;
class DependencyC2DependencyS;
class ActionC2ActionS;
class ParameterC2ParameterS;
class ComponentC2ComponentS;

class CMakeListsFilesParser;

namespace camitk {
	class ActionExtension;
	class ComponentExtension;
	class Property;
}
/**
* CEPInspection provides methods to explore the content of a CEP. 
* The best way is to give the links of the source directory and the build directory of the CEP.
* The method "GenerateXML" is able to generate a CEPContent.xml regarding the content of your CEP. TODO CS: rename it !
*/
class UPDATECED_API CEPInspection
{

public:
	CEPInspection();
	CEPInspection(QString srcPath, QString buildPath);
	~CEPInspection();

	
	// TODO: check the validity of paths. 
	QString		getPathSrc() const					{ return mPathSrc;  }
	void		setPathSrc(QString const str)		{ mPathSrc = str; }
	QString		getPathBuild() const				{ return mPathBuild;  }
	void		setPathBuild(QString const str)		{ mPathBuild = str;  }

	/**
	* Method that fill the dom tree with informations finding in the CEP.
	*/
	bool process();

protected:

	void init();

	/**
	* return true if the CEP gets already a CEPContent.XML file, false otherwise
	*/
	bool findFileExistingCEPContent();

	bool findFileCMakeLists();

	bool findFileDynamicLibraries();
	/**
	* Search recursively in the directory dirPath the file containing fileName.
	* Update the filePathList with the entire path of the file, if found.
	* TODO: manage several extensions (make a filter like Qt)
	*/
	void findFileInDirectory(QString fileName, QString dirPath, QStringList & filePathList);

	CMakeListsFilesParser* parseCmakeListFile(QString const file);

	bool loadExistingCepContentFile();

	bool loadExistingDynamicLibraries(QFileInfo file);

	

	/**
	* Write the domTree in a file
	*/
	bool writeDomTreeInAFile();

	/**
	* Method that update the dom tree of the CEP using informations from existing dom tree and CMakeList file.
	*/
	void updateCamiTKExtensionProject	(CepC2CepS* cep, CMakeListsFilesParser* parser);
	void updateActionExtension			(ActionExtC2ActionExtS* actExt, CMakeListsFilesParser* parser, camitk::ActionExtension* camitkActExt = 0); 
	void updateComponentExtension		(ComponentExtC2ComponentExtS* compExt, CMakeListsFilesParser* parser, camitk::ComponentExtension* camitkCompExt = 0);
	void updateLibrary					(LibraryC2LibraryS* lib, CMakeListsFilesParser* parser);
	void updateApplication				(ApplicationC2ApplicationS* app, CMakeListsFilesParser* parser);

	QList<DependencyC2DependencyS*> createListDependenciesFromCMakeFile(CMakeListsFilesParser* parser);
	QList<ActionC2ActionS*> createListActionsFromBuildLibrary(camitk::ActionExtension* camitkActExt);
	QList<ComponentC2ComponentS*> createListComponentsFromBuildLibrary(camitk::ComponentExtension* camitkCompExt);
	ParameterC2ParameterS* createParameterFromCamiTKProperty(camitk::Property* camitkProp);

	/**
	* Build (a map) of correspondancies betwwen CMakeList.txt files and dynamic libraries. 
	*/
	void buildCorrespondanciesMap();

	QMap<CMakeListsFilesParser*, QFileInfo> getMap() const { return mCorrespondancesMap;  }
	
private:

	/**
	* Internal method that do the same thing, but avoid duplicated code. 
	*/
	bool findFileAndFillList(QString fileName, QString dirPath, QStringList &list);

	/**
	* Map that etablish a correspondancy between the CMakeFile (ie the parsing CMake file) and the build  library.
	* As this library cannot load directly libraries, it is just a link to the file. 
	* If no link, then the library of the corresponding CMake is not built. 
	*/
	QMap<CMakeListsFilesParser*, QFileInfo> mCorrespondancesMap;

	CepC2CepS *mConvertor;

	QString mPathSrc;
	QString mPathBuild;

	QStringList mListCEPContentFile;
	QStringList mListCMakeFile;
	QStringList mListDynamicLibrariesFile;
};