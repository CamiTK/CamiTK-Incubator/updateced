#include <QtTest/QtTest>
#include "unit_tests_Qt.h"
//QTEST_MAIN(TestCep)
//QTEST_MAIN(TestActionExtension)
#include <Application.h>

int main(int argc, char *argv[]) \
{
	QStringList testCmd;
	QDir testLogDir;
	testLogDir.mkdir("UnitTest_Results");
	testCmd << " " << "-o" << "UnitTest_Results/test_log.txt";

	camitk::Application app("TestCatalogueEditor", argc, argv, false);

	TestCepInspection mTestCep;
	int res = QTest::qExec(&mTestCep, testCmd);

	return res;
}
