/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2016 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// CamiTK stuff
#include <Core.h>
#include <Application.h>

// includes from std
#include <iostream>

// CLI stuff
#include "CommandLineOptions.hxx"

#include <CEPInspection.h>

// description of the application. Please update the manpage-prologue.1.in also if you modify this string.
const char* description="Please visit http://camitk.imag.fr for more information.\n"
                        "(c) Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525\n\n"
                        "camitk-updatecd aims at creating the CamiTK Extension Project description\n"
                        "file (.ced) from the CEP source and build directories.";

// usage for this application
void usage(char *appName) {
    std::cerr << appName << std::endl;
    std::cerr << std::endl;
    std::cerr << "Usage: " << appName << " [options]" << std::endl;
    std::cerr << "Built using " << camitk::Core::version << std::endl;
    std::cerr << std::endl;
    std::cout << description << std::endl;
    std::cout << std::endl;
    std::cerr << "Options:" << endl;
    options::print_usage(cerr);
}

int main(int argc, char *argv[]) {
    try {
        int end; // End of options.
        options o(argc, argv, end);

        // if specific help or no options provided
        if (o.help()) {
            usage(argv[0]);
            return EXIT_SUCCESS;
        } else {
            // print the CamiTK version
            if (o.version()) {
                std::cout << argv[0] << " build using " << camitk::Core::version << std::endl;
                return EXIT_SUCCESS;
            } else {
                // source mandatory
                if (o.src_dir().empty()) {
                    std::cout << "Argument error: Please provide a CEP source directory" << std::endl << std::endl;
                    usage(argv[0]);
                    return EXIT_FAILURE;
                } else {
                    // now we can work!
                    // for the translation from argument to QString, see http://qt-project.org/doc/qt-4.8/qcoreapplication.html#accessing-command-line-arguments

                    // TODO use autoload extensions without a camitk application
                    camitk::Application app("updateced", argc, argv, false);

                    CEPInspection* mCepInspection = new CEPInspection();

                    mCepInspection->setPathSrc(QString::fromLocal8Bit(o.src_dir().c_str()));
                    if (!o.build_dir().empty())
                        mCepInspection->setPathBuild(QString::fromLocal8Bit(o.build_dir().c_str()));

                    bool success = mCepInspection->process();
                    delete mCepInspection;
                    
                    if (success)
                        return EXIT_SUCCESS;
                    else
                        return EXIT_FAILURE;
                }
            }
        }
    } catch (const cli::exception& e) {
        cerr << e << endl;
        usage(argv[0]);
        return EXIT_FAILURE;
    } catch (camitk::AbortException& e) {
        std::cout << argv[0] << " aborted..." << std::endl << "camitk AbortException:" << std::endl << e.what() << std::endl;
        return EXIT_FAILURE;
    } catch (std::exception& e) {
        std::cout << argv[0] << " aborted..." << std::endl << "std AbortException:" << std::endl << e.what() << std::endl;
        return EXIT_FAILURE;
    } catch (...) {
        std::cout << argv[0] << " aborted..." << std::endl << "Unknown Exception" << std::endl;
        return EXIT_FAILURE;
    }

}
